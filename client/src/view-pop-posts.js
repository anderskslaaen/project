import { LitElement, html, css,} from 'lit-element';


export class ViewPosts extends LitElement {

    static get properties() {
        return {
            msg: String,
            data: String
        }
    }

    constructor() {
        super();
        this.allPosts();
        this.msg = '';
    }

    static get styles() {
        return [
            css`
                #DIN CSS HER
            `,
        ]
    }

    render() {
        //console.log(this.data)
        if (!this.data) {
            return html`
            <h4>Loading...</h4>
            `;
        }else if(this.data.length >= 3) {
            this.data = this.data.reverse();
            return html `
            <ul class="posts">
            <h2 class="mb-4">Most recent Post from our great community!</h2>
            <li class="title">${this.data[0].title}</li>
            <li class="content">${this.data[0].content}</li>
            <br>
            <li class="title">${this.data[1].title}</li>
            <li class="content"> ${this.data[1].content}</li>
            <br>
            <li class="title">${this.data[2].title}</li>
            <li class="content">${this.data[2].content}</li>
            </ul>
            `
        } else{
            return html`
            <h4>Sorry not enough posts to display</h4>
            `;
            }
        }

    allPosts(){
        var output = '';
        fetch(`${window.MyAppGlobals.serverURL}getPosts`)
        .then((res) => res.json())
        .then((data) => {
        console.log(data);
        this.data = data;
        });
    }


}
customElements.define('view-pop-posts', ViewPosts);