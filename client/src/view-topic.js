import { LitElement, html, css, query,} from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

import './view-sidebar.js';
import './view-pop-posts.js';

var str = window.location + '';
var id = str.split('?');
id.shift();


export class ViewTopic extends LitElement {

    static get properties() {
        return {
            data: String,
            list: String
        }
    }

    constructor() {
        super();
        this.list = '<li class="title">If you see this refresh the page!</li>';
        this.topicPosts();
    }

    static get styles() {
        return [
            css`
                #DIN CSS HER
                `,
        ]
    }

    render() {
        console.log(this.list);
            return html`
            <div class="container">
			<div class="navbar">
				<h3 style="margin-right: auto;"><a href="/" style="text-decoration: none;">SPRÆTTKØNN.no</a></h3>
				<input type="text" placeholder="Search..">
				<h3><a href="/" >Login</a> <a href="reg.html">Register</a></h3>
			</div>
			<div class="sidebar2">
				<h4><a href="#makepost">Post something</a></h4>
				<hr>
				<h4>Your profile</h4>
			</div>
            <div class="sidebar1">
				<h4>Topics:</h4>
				<ul>
					<view-sidebar></view-sidebar>
				</ul>
			</div>
			<div class="main">
            <ul class="posts">
            ${unsafeHTML(this.list)}
            </ul>
			</div>
			<footer class="footer">
				<p>A website solution created by Kollektivet AS</p>
			</footer>
		</div>
		<div id="outlet"></div>
            `;
    }

    // gets all posts for topic
    topicPosts(){
        var output = '';
        fetch(`${window.MyAppGlobals.serverURL}topics/${id}`)
        .then((res) => res.json())
        .then((data) => {
        this.data = data;
        this.createList();
        });
    }

    // creaates html lists of posts
    createList(){
        this.list = '';
        this.data.forEach(element => {
            this.list += `<li class="title">${element.title}</li>
            <li class="content">${element.content}</li>
            <br>
            `

        });
    }
}
customElements.define('view-topic', ViewTopic);