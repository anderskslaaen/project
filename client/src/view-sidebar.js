import { LitElement, html, css,} from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js'

export class ViewSidebar extends LitElement {

    static get properties() {
        return {
            responce: String,
            list: String
        }
    }

    constructor() {
        super();
        this.list = '<li><a href="/nyttTopic">New Topic</a></li><br>'
        this.allTopics();
        this.responce = '';
    }

    static get styles() {
        return [
            css`
                #DIN CSS HER
            `,
        ]
    }

    render() {
        if (!this.responce) {
            return html`
            <h4>Loading...</h4>
            `;
        }else {
            return html `
            ${unsafeHTML(this.list)}
            `
        }
    }

    // gets all topics
    allTopics(){
        fetch(`${window.MyAppGlobals.serverURL}getTopics`)
        .then((res) => res.json())
        .then((data) => {
        this.responce = data;
        this.createList();
        });
    }

    // creates html list of topics
    createList(){
        this.responce.forEach(element => {
            this.list += `<li><a href="topics/?${element.TID}">${element.title}</a></li>`
        });

    }


}
customElements.define('view-sidebar', ViewSidebar);