import { LitElement, html, css,} from 'lit-element';

import './view-sidebar.js';
import './view-pop-posts.js';


export class ViewNewTopic extends LitElement {

    static get properties() {
        return {
			data: String
        }
    }

    constructor() {
		super();
		
    }

    static get styles() {
        return [
            css`
                #DIN CSS HER
            `,
        ]
    }

    render() {
            return html`
            <div class="container">
			<div class="navbar">
				<h3 style="margin-right: auto;"><a href="/" style="text-decoration: none;">SPRÆTTKØNN.no</a></h3>
				<input type="text" placeholder="Search..">
				<h3><a href="/" >Login</a> <a href="reg.html">Register</a></h3>
			</div>
			<div class="sidebar2">
				<h4><a href="#makepost">Post something</a></h4>
				<hr>
				<h4>Your profile</h4>
			</div>
            <div class="sidebar1">
				<h4>Topics:</h4>
				<ul>
					<view-sidebar></view-sidebar>
				</ul>
			</div>
			<div class="main">
            <h1 style="text-align: center;">Legg til nytt topic</h1><br>
            <form action ="http://localhost:8081/nyttTopic" method="POST">
                <fieldset>
                    <label>Tittel</label>
                    <input type="text" id='title' name="title" required>

                    <button type="submit">Submit</button>
                </fieldset>
            </form>
			</div>
			<footer class="footer">
				<p>A website solution created by Kollektivet AS</p>
			</footer>
		</div>
		<div id="outlet"></div>
            `;
        }
}
customElements.define('view-new-topic', ViewNewTopic);