import {Router} from '@vaadin/router';
import '/src/view-home.js';
import '/src/view-topic.js';
import '/src/view-new-topic.js'

const outlet = document.getElementById('outlet');
const router = new Router(outlet);

router.setRoutes([
    {path: '/', component: 'view-home'},
    {path: '/index.html', component: 'view-home'},
    {path: '/topics/', component: 'view-topic'},
    {path: '/nyttTopic', component: 'view-new-topic'},
])