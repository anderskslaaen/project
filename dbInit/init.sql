-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Nov 14, 2020 at 03:51 PM
-- Server version: 10.5.8-MariaDB-1:10.5.8+maria~focal
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prog2053-proj`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `cid` bigint(8) NOT NULL,
  `post` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `comment` varchar(20000) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`cid`, `post`, `user`, `comment`) VALUES
(1, 12, 1, 'But testing is pretty useful though'),
(2, 44, 43, 'Modi ut et soluta deserunt. Saepe qui nesciunt illum quis in est. Quia dignissimos tenetur nam accusantium accusantium vitae libero.'),
(3, 58, 71, 'Repudiandae officia tempora dolore illum. Quis perspiciatis fugiat maiores inventore. Ut sit et velit debitis doloribus error. Culpa reiciendis soluta dolores libero fugit explicabo.'),
(4, 49, 91, 'Temporibus aut quibusdam nihil qui. Ipsum minima dolor facilis vel cupiditate. Consequatur ut hic dolorem minus repellendus.'),
(5, 43, 1, 'Id dolore ex et voluptatem id. Numquam molestiae eligendi voluptate. Suscipit vitae magni numquam corrupti. Sit voluptate eum dolorum tempore. Laudantium voluptas amet omnis est.'),
(6, 51, 34, 'Quo ut dolorem autem nihil esse. Est voluptatibus odio a aliquam. Et inventore quis tenetur eaque adipisci qui. Esse voluptates culpa placeat corporis enim dolorum.'),
(7, 28, 62, 'Magnam nihil qui in quam velit aliquid harum. Impedit excepturi molestias at eum illum. Tempore minus voluptatibus nostrum.');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pid` bigint(8) NOT NULL,
  `topic` int(11) NOT NULL,
  `user` bigint(8) NOT NULL,
  `title` varchar(200) COLLATE utf8_bin NOT NULL,
  `content` varchar(20000) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pid`, `topic`, `user`, `title`, `content`) VALUES
(1, 1, 95, 'Aut nam eos praesentium doloremque illum facilis.', 'Ab hic minima perferendis tempora aspernatur. Est consectetur magni qui nihil vel. Voluptas possimus voluptatem incidunt corporis et. Delectus in qui cupiditate quis vel sit.'),
(2, 2, 20, 'Accusantium labore reiciendis tempora debitis.', 'Earum dicta voluptatem in a. Eligendi est nulla minima. Asperiores accusamus ad quod dolorum in ut.'),
(3, 3, 55, 'Repudiandae accusamus quo tempora ullam rerum corporis ut.', 'Accusamus ipsum assumenda vitae nihil. Amet praesentium officia ab neque. Et explicabo harum nulla modi facilis qui architecto beatae.'),
(4, 4, 33, 'Ut quod maxime asperiores accusamus.', 'In sequi eos corporis porro. Accusantium corporis dolor architecto molestiae qui voluptas voluptates voluptatem. Sit eius temporibus amet iure dolorum qui.'),
(5, 1, 76, 'Aut eaque soluta aperiam perferendis sint.', 'Qui nulla quibusdam iusto. Mollitia modi fuga quo est voluptates dolores commodi. Quod tenetur at sint et quasi repellendus.'),
(6, 2, 66, 'Ea incidunt facere tempore.', 'Nobis occaecati consequatur perferendis sit impedit sed. Non atque esse optio consequatur officia. Est enim consequatur accusantium ratione temporibus. Sit et rem repellat aut illum dolore.'),
(7, 3, 89, 'Voluptatem assumenda similique eveniet similique voluptate.', 'Omnis sit reiciendis officia natus enim quia libero. Qui exercitationem hic qui earum nostrum. Odit sed est laudantium beatae.');

-- --------------------------------------------------------
--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `TID` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`TID`, `title`) VALUES
(1, 'Discussion'),
(2, 'Economics'),
(3, 'Education'),
(4, 'Windows');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` bigint(8) NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userType` enum('admin','moderator','user') COLLATE utf8_bin NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `email`, `password`, `userType`) VALUES
(1, 'bartell.martine@example.com', '40bcc6f6193986153cae1bb1c36668650a3d5f97', 'admin'),
(2, 'zcrona@example.net', '1f66d81577cd95514cedc8504d65ec8eff9c336a', 'moderator'),
(3, 'wgaylord@example.com', '3fcba21eebd2d09681515b4849d2bbeae566451e', 'user'),
(4, 'jturcotte@example.net', '933045bb8d5e3a822514cdfc1ec357251adffdcb', 'admin'),
(5, 'rhiannon66@example.org', 'a838c262bc3c9ad86b74d76e603521acd527ceec', 'moderator'),
(6, 'ansley.schaden@example.org', '9f42f34c0260c3ce60e17bc0852159c7d95d43ff', 'user'),
(7, 'arne.huel@example.org', '597b70e4637cfedd8e79b7c70709a61287083e00', 'admin'),
(8, 'cmcclure@example.org', '9434525ad74f9f4e59f7a4f212ce223c19ef0af4', 'admin'),
(9, 'felipa35@example.org', '7fa816135c494632dc0b979574864f358f617e0e', 'user'),
(10, 'bfeest@example.org', '29c2651c84fc7d360900ecd0ff95bbaaec075241', 'admin'),
(11, 'josie76@example.net', 'a13ea462dbcd7dd3b3b97b54620f46df98bbd86c', 'admin'),
(12, 'kaylie.kreiger@example.org', '1aecb3924d2470c0a5225ebb4bc2816a4d94167e', 'user'),
(13, 'cara.simonis@example.net', '1392d07010e6b1cd0930342965c3c575b1613f4a', 'admin'),
(14, 'mcole@example.org', '1ed090707e27ee9a66da3b06445c3d3c3294a10c', 'user'),
(15, 'hane.jerrold@example.net', '50ada3ddd15eaaeaa8c5f7fe7b1403150e6ebbcf', 'moderator'),
(16, 'ubaldo26@example.com', '8f479dac76b07ea8cd8e52389d78f455b9b7956e', 'user'),
(17, 'mckayla44@example.net', '54364b44cb3ef9e8ce2412ba0870bb14a6657c78', 'user'),
(18, 'lueilwitz.ashton@example.com', '1960bbb47229c727d1ec814fb932d48372fee1cf', 'moderator'),
(19, 'lesley.funk@example.net', '7d89b525044787dd0767b2c24b91da2b66098ee8', 'admin'),
(20, 'samir.kihn@example.net', '533cdfb77c5126023897d43b1cdc32444ebe5559', 'moderator'),
(21, 'stroman.davin@example.com', '37615c8b741a61931826e818512465fe204b9b9c', 'moderator'),
(22, 'kylie.lowe@example.org', '485fd3ba2fcdf90a76e7e05f84c1353618f32b8f', 'user'),
(23, 'loma75@example.org', '846b84fa1de6bf2b1aec1deb83bbb8a503d508e5', 'user'),
(24, 'rodriguez.emely@example.org', 'e317596948ccd18f7bdf44ddd62e23a1a139d4dd', 'user'),
(25, 'aufderhar.rudy@example.com', '78050e414e4d859ac40a04e47293be5fe8836180', 'admin'),
(26, 'kaela57@example.net', 'a15e74b7a943fb1e0973ae488bd4724aaa1560de', 'user'),
(27, 'prosacco.maximilian@example.com', 'ca50850ec75c07b6c5659d584b0152d654b868cc', 'user'),
(28, 'lyda92@example.net', '1082609051a58d2efd71ca4ea535411eef1443d6', 'moderator'),
(29, 'kayla06@example.net', '741ffc8d5e6e8bf80560f7099b36748aecc107bc', 'user'),
(30, 'sam44@example.net', 'f749aadbf5d1d1fd0b1ec46befd8a8e7b1260960', 'user'),
(31, 'ryann.kertzmann@example.com', 'e4ca9c6cf7255aea55a722bdb2e3097a959c653d', 'moderator'),
(32, 'ceasar.reichel@example.net', 'd6c022b55ea529e5d2c1bd5eb0ce2b1afdfb3fb3', 'user'),
(33, 'alyce22@example.net', '12b3c6a9e479dcd64f888000bffac75b07d7c1d7', 'user'),
(34, 'oglover@example.net', 'df361bfb216355fff061f9e73560d9838b75874e', 'user'),
(35, 'bcarroll@example.com', 'c73324e12880a036059ef8add4ba19c6a48f4869', 'moderator'),
(36, 'hkeebler@example.org', 'd580160bfe2e5e1840e11e8fa1e9d0b497cf2d76', 'moderator'),
(37, 'ggusikowski@example.net', '23b818b9d9a4cf6f97ed974c57582f8ea46b13a4', 'admin'),
(38, 'stuart62@example.org', '930059dc0d7c29510fec65c0291a396e32e36e03', 'moderator'),
(39, 'zena31@example.net', 'b40e86d5deb2bd4a2d10b7313c5e11d0c55cca8e', 'admin'),
(40, 'lokeefe@example.com', 'b0198d090f91be1413c648e30c17dc4ada756d51', 'user'),
(41, 'ward.conor@example.net', 'e52ab6c7d53ab2ce261dd5e66b70addb3f711df6', 'admin'),
(42, 'wilhelm36@example.net', '4fbacdefe98ba3f65e612faef04b4a2b72f2d968', 'admin'),
(43, 'teresa.friesen@example.com', '64e6e4810a305ce4e6f162debfb6807571778ba6', 'moderator'),
(44, 'timothy64@example.net', '5e67a7fa1d58ecd938afe6b4e63c30b4073d11e1', 'admin'),
(45, 'arvel.batz@example.org', '39c9cfad572e4a6e56d358fba096ea90d71943a7', 'user'),
(46, 'terdman@example.org', '4f68429e9f5e0496f191b25064fb90197ffcb06c', 'moderator'),
(47, 'prosacco.rachelle@example.net', 'da031be03d685bc6e53076649dab3705c57badc7', 'admin'),
(48, 'nicholaus68@example.net', '7db960fbb24cfb2d23b9046ba23771077e8e7e36', 'moderator'),
(49, 'lambert.bartell@example.net', '18e00f5fdec007ed1439fd3ff0e46c751cf83737', 'moderator'),
(50, 'connelly.rolando@example.net', 'f0582a7c4520a26ae8c8e2a8af93684e868a8a09', 'moderator'),
(51, 'elliot.watsica@example.org', '750fd09faebc6e3aba3de7246489033d13bb8784', 'user'),
(52, 'casper.padberg@example.org', '5df117d8568cd1c7b616bcc64ac2310cf5f1e881', 'admin'),
(53, 'francisco.keebler@example.com', 'ab26026a8802e8b057e8cfb9db95a282a4afdcb8', 'admin'),
(54, 'lynch.curtis@example.net', '59e930ea45e33da64c74b47353aa45ad00ab39c8', 'moderator'),
(55, 'purdy.retha@example.org', '27d65132f320194b274c57a629a6f0e5f8d98ab3', 'admin'),
(56, 'white.colten@example.net', '08897f1d2e969f8234aa58bf79d2bb6757fa8c1f', 'moderator'),
(57, 'mollie41@example.net', 'c837e0eb5645217d091a4c6a048d73ee2f6ee79e', 'moderator'),
(58, 'madaline57@example.org', '9dfaa92a62c5d87f1ef0939c3c8c7da6af41b04c', 'user'),
(59, 'chloe35@example.net', '1c6ecc66760be64d412159adf7870a7d67dcc030', 'moderator'),
(60, 'nayeli18@example.net', 'f888da01484f0b1bc6066e704b6fd79951467fb6', 'admin'),
(61, 'fblock@example.com', '4137598346e3494132de7f4fb4baf44363459605', 'admin'),
(62, 'reichert.tyrell@example.com', '84e280050fe703623c61de1569ca78e6de4a4538', 'moderator'),
(63, 'rbeer@example.org', '48279885764cf03b6d572df284ae0cd1a11c2f91', 'moderator'),
(64, 'emory16@example.org', 'f1e9e179514317c41665f3c36a49d0241700afb1', 'user'),
(65, 'lisa46@example.org', 'bae4c2c441ebf2e0aa782e8277369778017e68f6', 'admin'),
(66, 'pgutkowski@example.net', '2de02919349c8692078058565165ddcf4e771745', 'moderator'),
(67, 'leta.nolan@example.org', '26b5011b4dbe91f77029b9a20faa37fa7612eeb5', 'user'),
(68, 'oyundt@example.net', '8f51eb68b7e5b2a22fcca98039bcfc355ded599a', 'moderator'),
(69, 'alexa96@example.org', '16a89b58959d657927fc9d903bd3861e19a65d66', 'moderator'),
(70, 'gerhold.lazaro@example.net', '0574a3dd8e5dc60f4baff64e41e3940c4585c59b', 'user'),
(71, 'cremin.dena@example.com', 'b8cbd24d839fec336b98835df1fc2e21b2b54b55', 'user'),
(72, 'carmela05@example.com', '869953170cd94a7c610c0b18737759f376460bfb', 'moderator'),
(73, 'heaney.caleb@example.net', 'c702da0ce5c442f6d6d8fc2d2b6351179ed05821', 'admin'),
(74, 'barrows.alexandria@example.com', '0d418f1acf8afef37c3fbe5387a9e344e4cebf1a', 'admin'),
(75, 'mzboncak@example.org', '47ab423b2dee193192c78813bce9e2a436bcbe42', 'moderator'),
(76, 'vsatterfield@example.com', 'e6ef3745369fe17bc9aa79fbbeaae6b9c0506e78', 'user'),
(77, 'mberge@example.com', '76b1db0a56c2b6a510292e7dce534479cbaccc9d', 'admin'),
(78, 'finn41@example.com', '04450a4e59323517d8665e2c9ec73aed26679be3', 'moderator'),
(79, 'ykuhlman@example.net', 'fe1a15b9226dca7318a2824eb925c93f9266ba05', 'moderator'),
(80, 'kira.powlowski@example.org', '8ba54bf0d8f7046610cfe15673227c5a53c9825f', 'moderator'),
(81, 'nstamm@example.org', 'd16dbd5be3897dbcf233a45e6dc0334c0688f763', 'moderator'),
(82, 'wrobel@example.org', '05bffc2aa6bd75b26aac649416f4f0dda48dc869', 'admin'),
(83, 'rosella.brekke@example.com', 'b32aa64965468f64509f7dd8163e9419a095e9a2', 'moderator'),
(84, 'kwolf@example.com', '1028114516ea54ab09bcf106fd3bad2b25c8f2e1', 'user'),
(85, 'vwintheiser@example.org', '56d38135295072696295db3e38718d4b0cf403e6', 'moderator'),
(86, 'rutherford.ola@example.net', '92c62918371383655df58524bdce62221ef7c20c', 'admin'),
(87, 'pinkie.leffler@example.net', 'a16fb2671ab7d0fbc2cc1d27bc8acc05113aa23f', 'admin'),
(88, 'jovany.ebert@example.com', '37442ead1ea7861ad47d44036294f446340b56fe', 'user'),
(89, 'icremin@example.com', 'ed3922daee839091c41414bf825f38ea33e9baca', 'moderator'),
(90, 'huel.jonatan@example.net', '7a4eaa1c22655fd45e2ec8bacc47b9c560963206', 'moderator'),
(91, 'francisco71@example.org', '9253602e127aeff50a87ccb3bf67db994682ad26', 'admin'),
(92, 'marquise84@example.net', 'c7dd8fb44f332342f3b7d94623c9c4f4c4477d7a', 'user'),
(93, 'abbott.jaylin@example.com', 'bc7c1d9b644e76ef7117b6a8f2df62112466f4c6', 'admin'),
(94, 'runolfsson.percival@example.org', '5cb6c594493943d68003b6962d86f47a50f30a9f', 'user'),
(95, 'fay.marlen@example.org', '84928c27681a2ef10fca94d1be53e1381d8cff48', 'moderator'),
(96, 'greenfelder.tyrell@example.org', '6022a3459e857d3bb4654e1bd66d53bc7c270857', 'admin'),
(97, 'justice.wisoky@example.com', '8d663081bb8465e7d1d328a4d385df5611315340', 'moderator'),
(98, 'murray.karelle@example.net', '4565bcdafd8bfd6e1d576c085066fa259f06f435', 'admin'),
(99, 'logan07@example.org', '6701124a9562a07a74a4c615d0436459fb167378', 'user'),
(100, 'schiller.glenna@example.net', '446ac41a5b9bfb475854332e3f3e328eaa344728', 'moderator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `user` (`user`),
  ADD KEY `post` (`post`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `user` (`user`),
  ADD KEY `id` (`topic`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`TID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `cid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post`) REFERENCES `posts` (`pid`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`),
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`topic`) REFERENCES `topics` (`TID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
