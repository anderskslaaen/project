"use strict";

import express from 'express';
import query from 'express';
import path from 'path';
import mysql from 'mysql';
import {scryptSync} from 'crypto';
import bodyParser from 'body-parser';

//Some imports related to sessions that are commented since the session system didn't quite work

//import session from 'express-session';
//import redis from 'redis';
//import RedisStore from 'connect-redis';
//import Redis from 'ioredis';

//const client = new Redis({ password: 'secret' })
//const store = new RedisStore({ client })

const salt = 'a97f0sb067c098ed82'; //The salt used for hashing, we decided to store it in the file, instead of generating it for each password
                                   //since that would require to alter the database even more, and make a much more complicated login/register system
const app = express()
app.use(
  express.urlencoded({
    extended: true
  })
)

//The session that didn't quite work
/*app.use(
  session({
    store,
    name: 'sid',
    saveUninitialized: false,
    resave: false,
    secret: 'Topp hemmelig ting!',
    cookie: {
      maxAge: 1000 * 60 * 60 *2,
      sameSite: true,
      secure: process.env.NODE_ENV === 'production'
    }
  })
)*/

//Function for redirecting to login if no session is made
/*const redirectLogin = (req, res, next) => {
  if(!req.session.userId) {
    res.redirect("http://localhost:8080/login.html")
  } else {
    next()
  }
}*/
const PORT = 8081;

import { fileURLToPath } from 'url';
import { dirname } from 'path';
import { isBuffer } from 'util';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

app.listen(PORT, () => {
  console.log('Running...');
});


app.set('view-engine', 'html')
app.use(express.static(path.resolve() + '/server'));
app.use(express.urlencoded({extended: false}));


var db = mysql.createConnection({
  host: "db",
  user: "admin",
  password: "password",
  database: 'prog2053-proj'
});

db.connect(function (err) {
  if (err) {
    throw err;
  }
  console.log("Connected!");
});

app.get('/', (req, res) => {
  res.send("Hello world");
})

//Function for users to log in
app.post('/login', function (req, res) {
  var email = req.body.email;                                                   //Fetches both email and hashed password
  var hashedPassword = scryptSync(req.body.password, salt, 32).toString("hex");
  if(email && hashedPassword) { //Checks that both an email and password are entered
    //A query to check if the email/hashed password pair exists in the database
    db.query('SELECT * FROM users WHERE email = ? AND password = ?', [email, hashedPassword], function(error, results, fields) {
      if(results.length > 0)  //if there is a match, user is redirected to the home page
        res.redirect("http://localhost:8080/index.html");
      else {  //else, the user is just redirected back to login
        res.redirect("http://localhost:8080/login.html");
      }
    });
  }
});

//Function for users to register

app.post('/register', function (req, res) {
  var email = req.body.email;                                                   //Fetches both email and hashed password
  var hashedPassword = scryptSync(req.body.password, salt, 32).toString("hex"); 
   db.query('SELECT * FROM users WHERE email = ?', [email], function(error, results, fields) {  //Query to see id email allready exists
    if(results.length > 0) //If email exists, the user is redirected back to register
      res.redirect("http://localhost:8080/register.html");
    else {  //else, hashed password and email is inserted to the database, and user is redirected to the login page
      db.query("INSERT INTO users(email, password, userType) VALUES('"+email+"', '"+hashedPassword+"', 'user')");
      res.redirect("http://localhost:8080/login.html");
    }
  });
});


// for å få alle posts
app.get('/getPosts', function(req, res) {
  let sql = 'SELECT * FROM posts';
  
  db.query(sql , function (err, result) {
    if (err) {
      res.status(400).send('Error in database operation.');
    } else {
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(result));
    }
  });
});


// for å få spesifik post id byttes ut med pid
// noe lignende kan gjøres hvis vi vil ha sortering etter kategorier
app.get('/getPost/:id', function(req, res) {
  let sql = 'SELECT * FROM posts where pid = ?';
  
  db.query(sql, req.params.id, function (err, result) {
    if (err) {
      res.status(400).send('Error in database operation.');
    } else {
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(result));
    }
  });
});


// for å få posts fra spesifikk topic
app.get('/topics/:id', function(req, res) {
  let sql = 'SELECT posts.title, posts.content FROM posts INNER JOIN topics ON posts.topic = topics.TID WHERE topics.TID = ? ';

  db.query(sql, req.params.id, function (err, result) {
    if(err){
      res.status(400).send('Error in database operation.');
    } else {
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(result));
    }
  })
})

// for å vise alle users
app.get('/getUsers', function (req, res) {
  db.query('SELECT * FROM users', function (err, result) {
    if (err) {
      res.status(400).send('Error in database operation.');
    } else {
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(result));
    }
  });
});


// for å lage nytt topic
app.post('/nyttTopic', function(req,res){
  db.query("INSERT INTO topics(title) VALUES ('"+req.body.title+"')"), function(err,result){
    if(err){
      res.status(400).send('Error in database operation.');
    } else {
      res.writeHead(200,{ 'Content-Type' : 'application/json'});
      res.end(JSON.stringify(result));
    }
  };
  res.redirect("http://localhost:8080/index.html");
})


// for å vise alle topics, brukes for sidebaren
app.get('/getTopics',function (req,res){
  db.query('SELECT * FROM topics',function(err,result){
    if(err){
      res.status(400).send('Error in database operation.');
    }
    else{
      res.writeHead(200,{ 'Content-Type':'application/json'});
      res.end(JSON.stringify(result));
    }
  });
})


// for å fjerne topic
app.get('/delTopic/:id',function (req,res){
  let sql = 'DELETE FROM topics WHERE TID = ?';

  db.query(sql, req.param.id, function(err,result){
    if(err){
      res.status(400).send('Error in database operation.');
    }
    else{
      res.writeHead(200,{ 'Content-Type':'application/json'});
      res.end(JSON.stringify(result));
    }
  });
})
